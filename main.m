function main()
%%MAIN Generates all the simulations and analyses for the project. Make
%sure that the mbon_stereotypy_project folder is the current MATLAB folder
%
% Usage:
%   MAIN()

%%%% run all simulations
simulateallnetworkvariations

%%%% analyse all individual simulations
runanalysisallsimulations

%%%% generate simulation figures
generatefigures

end % main