# Description 

The repository contains all the source code and data files to reproduce the results

## Getting Started

Download the source files, extract to a new folder, and add this folder and all its subfolders to your MATLAB path

### Prerequisites

* MATLAB (this code has been tested in version r2016a)

## Running the code

* Please run the ```main.m``` file present in the folder. It will run all simulations, run analyses on these simulations and create the figure files

## Built With

* MATLAB (version r2016a)
* modified version of the **gramm** plotting package available at [piermorel/gramm](https://github.com/piermorel/gramm)

## Authors

* **ammportal** - *All files*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
